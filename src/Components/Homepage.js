import React from 'react'
import './Styles2.css'
import HoverableCard from './HoverableCard';
import Footer from './Footer'; 



function Homepage() {
  return (
    // <div>Homepage</div>
    <div>
      {/* Your component content goes here */}
      <div className="background-container"><h1>Welcome to Quick Learn Hub – Your Gateway to Personalized Education!</h1>
      <b>Quick Learn Hub is an innovative and dynamic education app designed to revolutionize the way you learn. 
Our platform is committed to providing a personalized and engaging learning experience for students of all ages,
 ensuring that education is not only accessible but also tailored to individual needs.
 In the fast-paced digital era, technology has revolutionized various aspects of our lives, and education is no exception.
  The advent of education apps has opened up new avenues for learning, making education more accessible,
  engaging, and personalized. One such groundbreaking app that is reshaping the landscape of education is [App Name].
   In this blog, we'll explore how this innovative education app is transforming the way we learn and why it has become a game-changer 
  in the world of education.</b></div>


  <div className='coursetext'>
    <div className='homepageheading'>
    <h1>Cutting-Edge Transformational Programs</h1>
    <b>Highly recommended and trusted by experienced professionals</b>
    </div>
    <div className='innercoursetext'>
    <div><img src={require('./Images/f1.webp')} alt='error' />
      <p>Learn to code like a pro through intense short-term programs. Best suited for graduates and young professionals seeking to launch their career in a hyper-competitive industry.</p></div>
    <div><img src={require('./Images/f2.png')} alt='error' />
      <p>AI and Data Science are the reigning technologies, bringing unparalleled power to business and economy. Build hands-on expertise in these cutting-edge technologies from top academic research institutions and global corporations.</p></div>
    <div><img src={require('./Images/f3.jpg')} alt='error' />
      <p>Join the future of financial and business innovation with expertise in FinTech, Blockchain, and Distributed Ledger Technologies. Learn from top management and technical institutions in India.</p></div>
    <div>
    <img src={require('./Images/f4.png')} alt='error' /><p>Learn from leaders in Cyber Security and Cyber Defence and develop hands-on expertise from India’s top-ranked institution. Best suited for professionals passionate about exploring disruptions in the cyber world.</p></div>
    <div><img src={require('./Images/f5.png')} alt='error' />
      <p>Pioneer the technology-led healthcare revolution with cutting-edge capabilities in AI for Digital Health and Imaging. Best suited for professionals creating technology solutions in eHealth, Telemedicine, Well-being, BioTech, Wearables and more.</p></div>
    <div><img src={require('./Images/f6.jpg')} alt='error' />
      <p>AI enables marketers to automate and optimize their marketing efforts that makes personalised marketing-at-scale, a present day reality. Get ready to leverage the power of AI and make smarter and data-driven marketing decisions.</p></div>
    <div><img src={require('./Images/f7.jpg')} alt='error' />
      <p>DPA is the evolution of the ways businesses operate. By automating business processes, organizations can accelerate digital transformation from end to end. Build intelligent automation solutions for your organization that will improve the overall effectiveness and efficiency.</p></div>
   <div><img src={require('./Images/f8.jpg')} alt='error' />
     <p>RPA is the fastest-growing powerful business process automation software. It enables you with tools to create your own software robots to automate any business process. Join the league of next-gen professionals by building capabilities in robotic process automation.</p></div>
    <div><img src={require('./Images/f9.jpg')} alt='error' />
      <p>Semiconductor chips are the engine and lifeblood of the future world of technology. Providing faster and more powerful chips is the key to emerging technologies fulfilling their potential. Build VLSI chip designing capabilities to power new-age technologies like AI, IoT, VR, Mobility, Cloud, and Analytics.</p></div>
    <div><img src={require('./Images/f10.jpg')} alt='error' />
      <p>There will be hardly anything left untouched by IoT’s influence, from smart homes, smart cars, smart healthcare to smart cities. But the industry needs professionals with expertise in the technologies and processes around IoT to fulfill its ambitions of interconnectivity. Build relevant capabilities and ride the next big technology wave.</p></div>
    <div>
      <img src={require('./Images/f11.jpg')} alt='error' />
      <p>Manufacturers are integrating technologies like IoT, Cloud Computing and Analytics, and AI/ML into their production facilities and throughout their operations. This helps them with real-time decision-making, enhanced productivity, flexibility, and agility. Gain in-depth knowledge of such technologies to lead the digital transformation in the sector.</p></div>
    </div>

  </div>
  <div>
    {/* cards */}
    <h3>Keep Up with Deep Tech Trends</h3>
    <div className="cards-container">
        <HoverableCard
          image={require('./Images/f1.webp')}
          description="ji to code like a pro through intense short-term programs. Best suited for graduates and young professionals seeking to launch their career in a hyper-competitive industry."
        />
        {/* Repeat the above line for each card with different images and descriptions */}
        <HoverableCard
          image={require('./Images/f1.webp')}
          description="ji to code like a pro through intense short-term programs. Best suited for graduates and young professionals seeking to launch their career in a hyper-competitive industry."
        />
         <HoverableCard
          image={require('./Images/f1.webp')}
          description="ji to code like a pro through intense short-term programs. Best suited for graduates and young professionals seeking to launch their career in a hyper-competitive industry."
        />
      </div>
  </div>
  <div>
      {/* Your other components/content */}
      <Footer />
    </div>
    </div>
  )
}

export default Homepage