import React, { useEffect, useState } from "react";
import { Button } from "reactstrap";
import axios from "axios";
import './Styles.css'
function Assignment() {
  let [users, setUsers] = useState(null);
  const assignment = async () => {
    try {
      let response = await axios.get("");
      console.log(response.data);
      setUsers(response.data);
    } catch (err) {
      console.log(err);
    }
  };
    useEffect(() => {
        assignment();
    }, []);
  return (
    <div>
      <table>
        <tr id="head">
          <th className="item">id</th>
          <th className="item">Email</th>
          <th className="item">password</th>
          <th className="item">Address</th>
          <th className="item">State</th>
          
        </tr>
      </table>
      {users &&
        users.map((item) => (
          <table id="body">
            <tr id="row">
              <td className="item">{item._id}</td>
              <td className="item">{item.Email}</td>
              <td className="item">{item.password}</td>
              <td className="item">{item.Address}</td>
              <td className="item">{item.State}</td>
              <td>
                <Button className="item1">update</Button>
              </td>
              <td>
                <Button className="item1">delete</Button>
              </td>
            </tr>
          </table>
        ))}
    </div>
  );
}

export default Assignment;