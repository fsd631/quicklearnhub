import React from 'react'
import { Card, CardBody, CardTitle, CardText,CardSubtitle } from 'reactstrap';

function CardsAbout() {
  return (
    <div>
        <div>
        <Card
  style={{
    width: '18rem'
  }}
>
  <CardBody>
    <CardTitle tag="h5">
      Card title
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
      Card subtitle
    </CardSubtitle>
  </CardBody>
  <img
    alt="Card cap"
    src="https://picsum.photos/318/180"
    width="100%"
  />
  <CardBody>
    <CardText>
      Some quick example text to build on the card title and make up the bulk of the card‘s content.
    </CardText>

  </CardBody>
</Card>
        </div>
    </div>
  )
}

export default CardsAbout