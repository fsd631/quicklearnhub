import React from 'react';

function YouTubeVideo() {
  const videoId = 'viHILXVY_eU';

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '500px' }}>
      <iframe
        title="YouTube Video"
        width="50%"
        height="400"
        src={`https://www.youtube.com/embed/${videoId}?frameBorder=0&allow=accelerometer;autoplay;clipboard-write;encrypted-media;gyroscope;picture-in-picture&allowFullScreen`}
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
    </div>
  );
}

export default YouTubeVideo;