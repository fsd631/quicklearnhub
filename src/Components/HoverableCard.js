// HoverableCard.js
import React, { useState } from 'react';

const HoverableCard = ({ image, description }) => {
  const [isHovered, setIsHovered] = useState(false);

  return (
    <div
      className={`hoverable-card ${isHovered ? 'hovered' : ''}`}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <img src={image} alt="error" />
      <p>{description}</p>
    </div>
  );
};

export default HoverableCard;
