// CardCarousel.jsx

import React, { useRef } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './Styles2.css'

const CardCarousel = () => {
  const sliderRef = useRef(null);

  const handleNext = () => {
    sliderRef.current.slickNext();
  };

  const handlePrev = () => {
    sliderRef.current.slickPrev();
  };

  const settings = {
    dots: true,
    infinite: true,
    speed: 2000, // Initial speed
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const increaseSpeed = () => {
    const newSpeed = settings.speed - 500; // Adjust the decrement value based on your preference
    sliderRef.current.slickSetOption('speed', newSpeed, true);
  };

  return (
    <div className="card-carousel-container">
      <Slider ref={sliderRef} {...settings}>
        <div className="card">
          <img src={require('./Images/A1.jpg')} alt="Card 1" />
        </div>
        <div className="card">
          <img src={require('./Images/A2.jpg')} alt="Card 2" />
        </div>
        <div className="card">
          <img src={require('./Images/A3.webp')} alt="Card 3" />
        </div>
        {/* Add more cards as needed */}
      </Slider>

      <div className="controls">
        <button onClick={handlePrev}>&lt;</button>
        <button onClick={handleNext}>&gt;</button>
        <button onClick={increaseSpeed}>Increase Speed</button>
      </div>
    </div>
  );
};

export default CardCarousel;
