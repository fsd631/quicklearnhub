// CardComponent.js
import React from 'react';
import { Card, CardBody, CardTitle, CardText } from 'reactstrap';

const CardComponent = ({ title, subtitle, text, buttonText }) => {
  return (
    <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://picsum.photos/300/200"
  />
  <CardBody>
    <CardTitle tag="h5">
    Personalized Learning Experience
    </CardTitle>  
    <CardText>
    understands that every learner is unique, with different strengths, weaknesses, and learning styles. The app leverages
     advanced algorithms to tailor learning experiences to individual needs. Through personalized learning paths,
      students can progress at their own pace, ensuring a deeper understanding of the material.
     This adaptability not only fosters a sense of empowerment but also maximizes the efficiency of the learning process.
    </CardText>
    
  </CardBody>
</Card>


  );
};

export default CardComponent;
