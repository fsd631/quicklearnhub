import React, { useState } from 'react';

function Contact() {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [message, setMessage] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    // Handle form submission logic here
    console.log('Name:', name);
    console.log('Email:', email);
    console.log('Phone:', phone);
    console.log('Message:', message);
  };

  return (
    <div className='bgimg'>
      <div className='contactelement'>
      <h1 className='cc'>Drop us a line</h1>

      <div>
      <form onSubmit={handleSubmit}>
      <div className='formgroup'>
      <label className='cc'>
          Name:
          <input
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </label>
      </div>


     <div className='formgroup'>
     <label className='cc'>
          Email:
          <input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </label>
     </div>



     <div className='formgroup'>
     <label className='cc'>
          Phone:
          <input
            type="tel"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          />
        </label>
     </div>


    <div className='formgroup'>
    <label className='cc'>      
              <textarea
                value={message}
                onChange={(e) => setMessage(e.target.value)}
                placeholder="Enter your message here..."
              />
    </label>
    </div>

        <div className='formgroup'>
        <button type="submit">Submit</button>
        </div>

      </form>
      </div>
    </div>
    </div>
  );
}

export default Contact;