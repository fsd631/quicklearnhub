// import React, { useState } from 'react';
// import { Button,Input,Label,Col ,Row,FormGroup,Form} from 'reactstrap';
// import './Styles2.css'

// import axios from 'axios';
// import { useNavigate } from 'react-router-dom';
// function Register(args) {
  
//   const navigate = useNavigate()

//   let [Formdata,setFormdata] =useState({
//     email:"",
//     password:"",
//     address:"",
//     address2:"",
//     city:"",
//     state:"",
//     zip:""
//   });
//   const handleSubmit = async(e)=>{
//     e.preventDefault();
//     try {
//        let response = await axios.post("http://localhost:3002/register", Formdata)
//       console.log(response)
//       alert(response.data)
//       setFormdata("")
//       if (response.status === 200) {
//         navigate('/Login')
//       }

//       setFormdata({
//        email: "",
//      password: "",
//      address: "",
//      city: "",
//      state: "",
//      zip: ""
//       })
     

//    } catch (err) {
//        console.log(err)
//    }

//  }
//  const handleFormdata = (e) => {
//      let { name, value }=e.target
//      setFormdata({
//          ...Formdata,
//          [name]:value
//      })
//      console.log(Formdata)
//  }
  

//   return (
//     <div>
//      <Form onSubmit={handleSubmit}>
//   <Row>
//     <Col md={6}>
//       <FormGroup>
//         <Label for="exampleEmail">
//           Email
//         </Label>
//         <Input
//           id="exampleEmail"
//           name="email"
//           placeholder="@gmail.com"
//           type="email"
//           value={Formdata.email}
//           onChange={handleFormdata}
//           required
//         />
//       </FormGroup>
//     </Col>
//     <Col md={6}>
//       <FormGroup>
//         <Label for="examplePassword">
//           Password
//         </Label>
//         <Input
//           id="examplePassword"
//           name="password"
//           placeholder="password"
//           type="password"
//           value={Formdata.password}
//           onChange={handleFormdata}
//           required
//         />
//       </FormGroup>
//     </Col>
//   </Row>
//   <FormGroup>
//     <Label for="exampleAddress">
//       Address
//     </Label>
//     <Input
//       id="exampleAddress"
//       name="address"
//       placeholder="1234 Main St"
//       value={Formdata.address}
//       onChange={handleFormdata}
//           required
//     />
//   </FormGroup>
//   <FormGroup>
//     <Label for="exampleAddress2">
//       Address 2
//     </Label>
//     <Input
//       id="exampleAddress2"
//       name="address2"
//       placeholder="Apartment, studio, or floor"
//       value={Formdata.address2}
//       onChange={handleFormdata}
//           required
//     />
//   </FormGroup>
//   <Row>
//     <Col md={6}>
//       <FormGroup>
//         <Label for="exampleCity">
//           City
//         </Label>
//         <Input
//           id="exampleCity"
//           name="city"
//           value={Formdata.city}
//           onChange={handleFormdata}
//           required
//         />
//       </FormGroup>
//     </Col>
//     <Col md={4}>
//       <FormGroup>
//         <Label for="exampleState">
//           State
//         </Label>
//         <Input
//           id="exampleState"
//           name="state"
//           value={Formdata.state}
//           onChange={handleFormdata}
//           required
//         />
//       </FormGroup>
//     </Col>
//     <Col md={2}>
//       <FormGroup>
//         <Label for="exampleZip">
//           Zip
//         </Label>
//         <Input
//           id="exampleZip"
//           name="zip"
//           value={Formdata.zip}
//           onChange={handleFormdata}
//           required
//         />
//       </FormGroup>
//     </Col>
//   </Row>
 
//   <Button type='submit' className='registerbutton'> 
//     Register
//   </Button>
// </Form>
//     </div>
//   );
// }

// export default Register;




import React, { useState } from 'react';
import { Button, Input, Label, Col, Row, FormGroup, Form } from 'reactstrap';
import './Styles2.css'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import loginimage from './Images/A2.jpg'


function Register() {

  const navigate = useNavigate()

  let [Formdata, setFormdata] = useState({
    email: "",
    password: "",
    address: "",
    address2: "",
    city: "",
    state: "",
    zip: ""
  });
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let response = await axios.post("http://localhost:3002/register", Formdata)
      console.log(response)
      alert(response.data)
      setFormdata("")
      if (response.status === 200) {
        navigate('/LoginForm')
      }

      setFormdata({
        email: "",
        password: "",
        address: "",
        city: "",
        state: "",
        zip: ""
      })


    } catch (err) {
      console.log(err)
    }

  }
  const handleFormdata = (e) => {
    let { name, value } = e.target
    setFormdata({
      ...Formdata,
      [name]: value
    })
    console.log(Formdata)
  }


  return (
    <div className='register-main-container'>
        
      <div className='register-container'>
        <div className='login-image'><img style={{width:'100%',height:'100%'}} src={loginimage} alt='' /></div>

      <div className='register-form'>
        <Form className='register-form-container'onSubmit={handleSubmit}>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="exampleEmail">
                  Email
                </Label>
                <Input
                  id="exampleEmail"
                  name="email"
                  placeholder="@gmail.com"
                  type="email"
                  value={Formdata.email}
                  onChange={handleFormdata}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="examplePassword">
                  Password
                </Label>
                <Input
                  id="examplePassword"
                  name="password"
                  placeholder="password"
                  type="password"
                  value={Formdata.password}
                  onChange={handleFormdata}
                  required
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Label for="exampleAddress">
              Address
            </Label>
            <Input
              id="exampleAddress"
              name="address"
              placeholder="1234 Main St"
              value={Formdata.address}
              onChange={handleFormdata}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleAddress2">
              Address 2
            </Label>
            <Input
              id="exampleAddress2"
              name="address2"
              placeholder="Apartment, studio, or floor"
              value={Formdata.address2}
              onChange={handleFormdata}
              required
            />
          </FormGroup>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="exampleCity">
                  City
                </Label>
                <Input
                  id="exampleCity"
                  name="city"
                  value={Formdata.city}
                  onChange={handleFormdata}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={4}>
              <FormGroup>
                <Label for="exampleState">
                  State
                </Label>
                <Input
                  id="exampleState"
                  name="state"
                  value={Formdata.state}
                  onChange={handleFormdata}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={2}>
              <FormGroup>
                <Label for="exampleZip">
                  Zip
                </Label>
                <Input
                  id="exampleZip"
                  name="zip"
                  value={Formdata.zip}
                  onChange={handleFormdata}
                  required
                />
              </FormGroup>
            </Col>
          </Row>

          <Button type='submit'>
            Sign in
          </Button>
        
         
        </Form>
        </div>
        </div>
        </div>
  )
}

export default Register