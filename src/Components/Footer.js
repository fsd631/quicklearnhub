// Footer.jsx

import React from 'react';
 // Import your CSS file

function Footer() {
  return (
    <div className="footer-container">
      <p>&copy; 2024 Your Company. All rights reserved.</p>
      {/* Add more footer content as needed */}
    </div>
  );
}

export default Footer;
