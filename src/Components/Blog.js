import React from 'react'
import CardComponent from './CardComponent';

function Blog() {
  return (
    <div>
      <h3>Introduction</h3>
      <i>In the ever-evolving landscape of education,
      the advent of technology has ushered in a new era of learning,
      making education more accessible, personalized, and engaging.
      Among the myriad of educational apps, QuickLearnHub stands out as a beacon of innovation,
      transforming the way students and learners of all ages experience education. In this blog post,
      we will delve into the features and benefits that make QuickLearnHub 
      a game-changer in the educational technology space.</i>
      
    
      <div>
     
      <CardComponent
        title="Card 1"
        subtitle="Subtitle 1"
        text="This is the description of Card 1."
        buttonText="Click me"
      />
      </div>
     

    </div>
  )
}

export default Blog