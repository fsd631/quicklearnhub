import React from 'react'
import YouTubeVideo from './YouTubeVideo'
import Footer from './Footer'; 

function Courses() {
  return (
    <div>
      <h2>How this course works</h2>

    <div className='coursegrid'>

    <div>
    <img src={require('./Images/c1.webp')} alt='error' style={{ height: '200px', width: '300px' }} />
     <h3>Learn industry tools</h3>
      <p>Work on your projects using industry-standard tools, such as Figma, Adobe XD, or Sketch.</p>
     </div>

     <div>
     <img src={require('./Images/c2.webp')} alt='error' style={{ height: '200px', width: '300px' }} />
     <h3>Weekly feedback sessions</h3>
      <p>Attend mentor-led webinars designed to provide valuable feedback and expert guidance for your projects.</p>
     </div>
     
     <div>
     <img src={require('./Images/c3.webp')} alt='error' style={{ height: '200px', width: '300px' }} />
     <h3>Build a Portfolio</h3>
      <p>Develop your UX portfolio and demonstrate your job readiness through a series of real-world projects</p>
      </div>

    </div>
    <YouTubeVideo />
    <div>
      {/* Your other components/content */}
      <Footer />
    </div>
    </div>
  )
}

export default Courses