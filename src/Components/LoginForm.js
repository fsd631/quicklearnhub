import React, { useState } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import './Styles2.css'
import axios from 'axios';
import loginimage from './Images/reg.webp'
import { Link } from 'react-router-dom';
function Login(args) {

  let [email, setemail] = useState("");
  let [password, setpassword] = useState("");



  const handleSubmit = async (e) => {
    e.preventDefault();
    // alert(Email+" "+Password)

    try {
      let response = await axios.post("http://localhost:3005/login", { "email": email, "password": password })
      console.log(response)
      alert(response.data)
    } catch (err) {
      console.log(err)
    }
  }
  return (
    <div className='login-main-cointainer'>
     
      <div className='login-container'>
        <div className='login-image'><img style={{width:'100%',height:'100%'}} src={loginimage} alt='' /></div>
        <div className='login-form'>
          <Form className='login-form-container' onSubmit={handleSubmit}>
            <FormGroup>
              <Label
                for="exampleEmail"
                hidden
              >
                Email
              </Label>
              <Input
                id="exampleEmail"
                name="email"
                placeholder="Email"
                type="email"
                value={email}
                onChange={(e) => { setemail(e.target.value) }}
                required
              />
            </FormGroup>
            {' '}
            <FormGroup>
              <Label
                for="examplePassword"
                hidden
              >
                Password
              </Label>
              <Input
                id="examplePassword"
                name="password"
                placeholder="Password"
                type="password"
                value={password}
                onChange={(e) => { setpassword(e.target.value) }}
                required
              />
            </FormGroup>
            {' '} <br/>
            {/* <Link to=''>Forgot your password?</Link>{' '} <br/> */}
            <Button type='submit'>
              Submit
            </Button>{' '} <br/><br/>
            <div style={{display:'flex',justifyContent:'center',gap:'8%'}}>Don’t have an account yet? <Link to="/Register"> <Button type='submit'>
           Register
          </Button></Link></div>
          </Form>
         
        </div>
      </div>
    </div>
  );
}

export default Login;


