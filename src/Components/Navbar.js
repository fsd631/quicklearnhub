

import React, { useState } from 'react';
import './Styles.css';
import './Styles2.css';

function Navbar() {
  const [mobileMenu, setMobileMenu] = useState(false);

  const toggleMobileMenu = () => {
    setMobileMenu(!mobileMenu);
  };

  return (
    <div className='Nav'>
      <div className='navbar-header'>
        <h1 className='homepageheading'>QuickLearnHub</h1>
      </div>
      <div id='mobile' onClick={toggleMobileMenu}>
        <i className={`fas ${mobileMenu ? 'fa-times' : 'fa-bars'}`}></i>
      </div>
      {mobileMenu && (
        <div className='navbar-links'>
          <ul>
            <li><a style={{ textDecoration: 'none' }} href="/">Home</a></li>
            <li><a style={{ textDecoration: 'none' }} href="/about">About</a></li>
            <li><a style={{ textDecoration: 'none' }} href="/courses">Courses</a></li>
            <li><a style={{ textDecoration: 'none' }} href="/contact">Contactus</a></li>
            <li><a style={{ textDecoration: 'none' }} href="/login">Login</a></li>
            <li><a style={{ textDecoration: 'none' }} href="/register">Register</a></li>
            <li><a style={{ textDecoration: 'none' }} href="/">Logout</a></li>
            {/* Add other menu items */}
          </ul>
        </div>
      )}
    </div>
  );
}

export default Navbar;
