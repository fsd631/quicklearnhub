import React from 'react'
import HoverableCard from './HoverableCard';
import CardCarousel from './CardCarousel';
import Footer from './Footer'; 

function About() {
  return (
    <div>
      


  
  <div className="about-container">
    
  <h2>Welcome to QuickLearnHub</h2>
      <p>
        QuickLearnHub is your fast-track to knowledge and skill acquisition. We're a dynamic 
        online learning platform dedicated to providing quick and effective learning solutions. 
        Our goal is to empower individuals with the tools they need to succeed in a rapidly 
        changing world.
      </p>
      <p>
        At QuickLearnHub, we understand the value of time, and that's why we've curated a 
        selection of concise and impactful courses. Whether you're a busy professional looking 
        to upskill, a student with a tight schedule, or someone eager to learn on the go, 
        QuickLearnHub has you covered.
      </p>
      <p>
        Our platform is designed for efficiency without compromising the depth of learning. 
        Dive into our courses, absorb the essentials, and apply your newfound knowledge 
        immediately. Learning should be accessible, enjoyable, and most importantly, quick!
      </p>
      <p>
        Join QuickLearnHub today and experience the joy of accelerated learning. Fast-track 
        your way to success with QuickLearnHub!
      </p>
  </div>
  

  
  <div>
    <h2>Keep up with Deep Tech trends</h2>
    <div className='abouttext' >
    
    

    </div>
   

    
  </div>
  <div>
      {/* Your other components/content */}
      <CardCarousel />
    </div>
  <div>
    {/* cards */}
    <h3>Keep Up with Deep Tech Trends</h3>
    <div className="cards-container">
        <HoverableCard
          image={require('./Images/f1.webp')}
          description="ji to code like a pro through intense short-term programs. Best suited for graduates and young professionals seeking to launch their career in a hyper-competitive industry."
        />
        {/* Repeat the above line for each card with different images and descriptions */}
        <HoverableCard
          image={require('./Images/f1.webp')}
          description="ji to code like a pro through intense short-term programs. Best suited for graduates and young professionals seeking to launch their career in a hyper-competitive industry."
        />
         <HoverableCard
          image={require('./Images/f1.webp')}
          description="ji to code like a pro through intense short-term programs. Best suited for graduates and young professionals seeking to launch their career in a hyper-competitive industry."
        />
      </div>
      </div>
      <div>
      {/* Your other components/content */}
      <Footer />
    </div>
    </div>
  )
}

export default About