
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';


import Navbar from './Components/Navbar';
import About from './Components/About';
import Courses from './Components/Courses';
import Blog from './Components/Blog';
import Contact from './Components/Contact';
import Homepage from './Components/Homepage';
import LoginForm from './Components/LoginForm';
import Register from './Components/Register'



function App() {
  return (
    <div>
      {/* <Navbar/> */}
      <BrowserRouter>
      <Navbar/>
      <Routes>
        <Route path='/' element={<Homepage/>} />
        <Route path='/about' element={<About/>} />
        <Route path='/courses' element={<Courses/>} />
        <Route path='/blog' element={<Blog/>} />
        <Route path='/contact' element={<Contact/>} />
        <Route path='/login' element={<LoginForm/>} />
        <Route path='/register' element={<Register/>} />
        
       
        

        
      </Routes>
      </BrowserRouter>
    
    </div>
  );
}


export default App;
